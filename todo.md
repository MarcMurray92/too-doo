07-Mar-2016

- Find which project I was actually working on
- Unbreak everything (URGH) That didn’t take long, for some reason I had pushed the repo with an empty todo.html directive template
- Write out detailed list of remaining tasks for core functionality before splitting into test interfaces.

__Styling__

- General Style Tasks
- Animate elements on show/hide
- Work on responsiveness.
- Create basic Logo for authenticity purposes.

__Add New Task__

- Seriously, the ‘Add New Task’ bit looks MANKY, need to sort it out. It’s throwing off the look of every other thing.

__Editing Mode Styles__

- Going to need to use ng-class to make the editing more consistent, want to KEEP the hover colour on the background of items being edited. At the moment the background turns white when you mouse off of it.
- Make ‘edit’ and ‘delete’ buttons disappear when in editing mode.
- Also need to sort the labels colours, at the minute they stay white whereas I want them transparent.
- Keep checkbox backgrounds white
- When in editing mode, preview the colour of a category in the dropdown somehow.

__Functionality__

- General Functionality Tasks
- Get started on sidebar sorting. Sort by category or/and date.
- Get $index issue fixed. Thanks for mentioning it by the way Treehouse.

__Editing Mode Functionality Tasks__

- Revert back to previous entry on clicking ‘cancel’
- Only update model on click ’Save' rather than ng-blink

__Add New Task Functionality Tasks__

- Incorporate date picker

__Code Factoring__

- Create separate, smaller directives. 
- New Task
- Todays 
- Tomorrows
- Later
- Sidebar

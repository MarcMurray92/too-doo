<link href="md.css" rel="stylesheet"></link>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

##Eyy Jason

Whats up is this form here needs validating:

````
<form class="add_todo form-inline form-group-lg" ng-submit="addTodo()">
    <input type="text" class="todo-text form-control" ng-model="todoText" size="30" placeholder="Create a New Task">
    <span class="select-style">
<select required ng-model="todoCategory" class="form-control custom">
        <option value="">Category</option>
            <option ng-repeat="category in categories" value="{{category.category}}" ng-bind="category.category"></option>
        </select>
    </span>
    <span>
<p class="input-group">
<input required type="text" class="form-control date-picker-select" 
uib-datepicker-popup="{{format}}" 
ng-model="todoDate" 
is-open="popup1.opened" 
datepicker-options="dateOptions" 
ng-required="true" close-text="Close" 
alt-input-formats="altInputFormats" /> <span class="input-group-btn"> <button type="button" class="btn btn-default" ng-click="open1()">
   <i class="fa fa-calendar-o"></i>
</button> </span> </p>
    </span>
    <input class="btn-primary btn-task form-control" type="submit" value="Add Task">
</form>
````

To validate you need to use the form name, eg:

```` 
<input type="text" class="todo-text form-control" ng-model="todoText" size="30" placeholder="Create a New Task" name="taskName">
````

but when I do that, the form won't submit anymore. literally when I add the name.

{{addTodo.taskName.$valid}}

Heres the code for submitting the form:

````
$scope.todos.unshift({
    name: $scope.todoText,
    done: false,
    date: $scope.todoDate,
    category: $scope.todoCategory
});
console.log($scope.todoDate)
$scope.todoText = '';
$scope.todoDate = '';
$scope.todoCategory = '';
};
````

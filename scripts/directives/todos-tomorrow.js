//todo.js
angular.module('todoListApp')
 .directive('tomo', function() {
   return {
    templateUrl: 'templates/todos-tomorrow.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});
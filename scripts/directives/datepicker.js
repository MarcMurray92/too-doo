/*global angular:true */
//todo.js
angular.module('todoListApp')
 .directive('datepicker', function() {
   return {
    templateUrl: 'templates/datepicker.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});
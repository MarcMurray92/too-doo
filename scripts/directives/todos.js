/*global angular:true */
//todo.js
angular.module('todoListApp')
 .directive('todos', function() {
   return {
    templateUrl: 'templates/todos.html',
    replace: true //added this line
    };
});
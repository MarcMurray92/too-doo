/*global angular:true */
//todo.js
angular.module('todoListApp')
 .directive('sidenav', function() {
   return {
    templateUrl: 'templates/sidenav.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});
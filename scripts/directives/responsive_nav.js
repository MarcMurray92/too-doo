/*global angular:true */
//todo.js
angular.module('todoListApp')
 .directive('responsivenav', function() {
   return {
    templateUrl: 'templates/responsive_nav.html',
    replace: true //added this line
    };
});
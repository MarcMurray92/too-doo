/*global angular:true */
//todo.js
angular.module('todoListApp')
 .directive('topbar', function() {
   return {
    templateUrl: 'templates/topbar.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});
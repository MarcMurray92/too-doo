/*global angular:true */

var app = angular.module('todoListApp');

app.controller('mainCtrl', function($scope, dataService) {
    $scope.filterDay = '';
    $scope.todos = [];
    $scope.categories = [{
        category: "work",
        id: 1
    }, {
        category: "life",
        id: 2
    }, {
        category: "chores",
        id: 3
    }, {
        category: "fitness",
        id: 4
    }];



    // Day Filter
    $scope.filterDays = function(day) {
        console.log('filter ', day);
        $scope.filterDay = day;
    };
    $scope.filters = {};

    $scope.selected = 0;

    // Track selected category filter
    $scope.select = function(index) {
        $scope.selected = index;
    };

    // Add new Category
    $scope.addCategory = function() {
        var categoryID = ($scope.categories.length);
        var categoryIDCounter = categoryID += 1;
        $scope.categories.push({ category: $scope.categoryText, id: categoryIDCounter });
        $scope.categoryText = '';
    };

    // Get Mock Task Date
    dataService.getTodos(function(response) {
        $scope.todos = response.data;
    });


    // Add new Task
    $scope.addTodo = function(form) {
        if (form.$valid) {
            form.$setPristine();
            $scope.todos.unshift({
                    name: $scope.todoText,
                    done: false,
                    date: $scope.todoDate,
                    category: $scope.todoCategory
                });
            $scope.todoText = '';
            $scope.todoDate = '';
            $scope.todoCategory = '';
        } else {
            console.log('get fucked');
        }
    };

    // Delete a Task 
    $scope.deleteTodo = function(todo, $index) {
        dataService.deleteTodo(todo);
        $scope.todos.splice($index, 1);
    };


    //---Calendar Stuff---//

    // Config for Datepicker
    $scope.dateOptions = {
        minDate: new Date(),
        showWeeks: false,
        showButtonBar: false
    };

    // Grab today and inject into field
    $scope.today = function() {
        $scope.todoDate = new Date();
    };

    // run today() function
    $scope.today();

    // setup clear
    $scope.clear = function() {
        $scope.todoDate = null;
    };

    // open the calendar
    $scope.open = function($event) {
        // $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.formats = ['dd-MMMM-yyyy'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.open1 = function() { $scope.popup1.opened = true; };
    $scope.popup1 = { opened: false };


    $scope.todoFilter = function(date) {
        return function(todo) {
            if (date === 'today' && todo.date === $scope.todaysDate) {
                return true;
            }
        };
    };


});


// date filtering
app.filter("filterTasksUsingDay", function() {
    return function(input, date) {
        if (date == '') {
            return input;
        }

        var filterDays = [];
        if (date == 'today') {
            filterDays.push(moment());
            console.log(moment());
        } else if (date == 'tomorrow') {
            filterDays.push(moment().add(1, 'day'));
        } else if (date == '7days') {
            filterDays.push(moment());
            for (i = 1; i < 7; i++) {
                filterDays.push(moment().add(i, 'day'));
            }
        }

        var filtered = [];
        angular.forEach(input, function(todo, index) {
            if (todo.date instanceof Date) {

                angular.forEach(filterDays, function(filterDay) {
                    // console.log(moment.duration(filterDay.diff(todo.date)).get("days"));
                    if (filterDay.isSame(todo.date, 'days')) {
                        filtered.push(todo);
                    }
                });
            }
        });

        return filtered;
    };
});

// filter for matching properties

app.filter('grabCatID', function() {
    return function(native, options, matchField, valueField) {
        if (options) {
            for (var i = 0; i < options.length; i++) {
                var o = options[i][matchField];
                if (o === native) {
                    return options[i][valueField];
                }
            }
        }
        return '';
    };
});

// datepicker settings

app.directive('datepickerPopup', function() {
    return {
        restrict: 'EAC',
        require: 'ngModel',
        link: function(scope, element, attr, controller) {
            //remove the default formatter from the input directive to prevent conflict
            controller.$formatters.shift();
        }
    };
});

angular.module("todoListApp")

.service('dataService', function($http) {

    // DB GET function
    this.getTodos = function(callback) {
        $http.get("mock/todos.json")
            .then(callback);
    };

    this.deleteTodo = function(todo) {
        console.log("The " + todo.name + " task has been deleted!");
        // other actual Database logic here
    };
    this.saveTodo = function(todo) {
        console.log("The " + todo.name + " task has been saved!");
        // other actual Database logic here
    };
});

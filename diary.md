ow s<link href="md.css" rel="stylesheet"></link>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<title>Honors Diary</title>


#Marc Murray Honours Diary

##Contents
1. <a href="#weekone">__Week One__</a>
2. <a href="#weektwo">__Week Two__</a>
3. <a href="#weekthree">__Week Three__</a>
4. <a href="#weekfour">__Week Four__</a>
5. <a href="#weekfive">__Week Five__</a>
6. <a href="#weeksix">__Week Six__</a>
7. <a href="#weekseven">__Week Seven__</a>

----

##__Week One__

####20/01/2015

#####Comparative Analysis

I chose 3 popular to do list applications to get an idea of standard interactions and layout conventions.
The full, detailed analysis can be found <a href="">here</a>

1. __Todoist__ takes a project based approach and makes use of a flat minimal aesthetic. There are multiple ways of categorizing tasks. <br> 
<br>
<img src="img/screenshots/todoist.png" alt=""> <br>
<br>
	- The use of a comforting blank slate is a great addition, adds to sense of completion. Considering creating a congratualtions animation for when the days tasks are complete. A simple arrow tick, or a more complex one including a mascot?

	- The categories navigation is simple and unintrusive.

	- Calender for adding tasks and its preset times is handy for quick additions, icons add a little character too.



2. __Wunderlist__ 

3. __Any.Do__ is layed out in a tile pattern. After some use I found this interface to be cumbersome. 
<br>
<img src="img/screenshots/anydo notes.png" alt="">
<br>
	- In particular the low contrast and thin lines resulted in an unclear division on sections. However, the addition of notes to tasks was interesting and worth considering. That said the overly minmal interface was ignoring many standard interface conventions like and 'okay' button. 
	- Although the content entered by the user may have been added dynamically, the user is given no sign that it has been entered.

#####User Personas



----

####23/01/2015

#####User Stories

Began User Stories, related to the User personas from last week. Available <a href="">here.</a>

#####User Flows

Initial User Flows designed, available <a href="">here</a>

#####Design & Development

Spent a few hours on treehouse learning how to animate SVG

Animations to consider

1. Task completion, circle spins, tick is filled, and as the tick 'explodes' away, the task fades out.
2. Hover states on desktop for tasks highlights what category the task is in to a higher degree.
3. On tablet and desktop incorperate a swipe based elastic SVG sidebar/off canvas navigation.

Interactions to consider

1. Tooltips on icons in task list.


Started high fidelity mockups

1. Card based layout for larger chunks of information, tabular for the smaller pieces.

----

####24/01/2015

High fidelity mockups almost done, structure is fairly straightforward but aesthetics are somewhat lacking.

__Available technology review for web animation.__

SVG
_SVG is a file extension for a vector graphics created by the W3C used to describe images through maths._
_SVG files can be embedded into web pages just like image tags, but for maximum flexibility it is best to paste the actual SVG code into the markup._


- Performant as hell
- CSS, SMIL, JS
	- CSS
		- _Simple, declarative, quick, easy, quite powerful._
	- SMIL
		- _SMIL support is waning, chrome will be dropping it soon. Mainly because similar results can be achieved from existing technology and SMIL has no in built advantages._
	- JS
		- _Powerful, access to all sorts of functionality that allows fine grain control of multiple elements. All the power of CSS & more._

Probably going to end up using GSAP for more detailed stuff.

Mock up

#####Still To Do:
1. Create custom icons for consitency, current icons are all over the shop.
2. Design logo, and choose brand colour to exclude from category colors.
3. Improve design of buttons (category button & add task button)
4. Prototype animations out.


----

####26/01/2015 - Meeting

__todo for next week:__

- finish high fidelity mockups

- look into interaction in Sketch

- Create high fidelity prototypes

- Perform initial User Tests of current Mockups


----

##__Week Two__

####26/01/2015

#####Prototyping

Started looking into using InVision to create interactive prototypes for quick & simple prototypes to perform user testing with.

Turns out Sketch and InVision can be synced and as such creating them is a breeze.
InVision seems simple enough, just need to create enough screens to demonstrate the apps functionality, only took maybe 30 minutes to get to grips ith InVision and create a list of the screens I will need.

Sketch actually has a plugin for a different prototyping app called Marvel.

I'll try them both out and see which I prefer.

For the purposes of these tests, the actual animations of the interface will be left out. These tests are simply to catch any user flow or comprehension issues, and to make sure the interface is as easy to use and understand as possible.
This will hopefully avoid the interfaces layout affecting the results of future tests.

Time now to improve the static interface.

#####Animation Brainstorming

Also been thinking more in depth about specific animations for this interface, in particular the blank slate/celebratory "you are fininshed!" animation. It may be very detailed, in which case creating it through SVG may be a big undertaking. Going to start sketching ideas for that, not sure where to go with it yet. A detailed illustration etc might benefit from the use of a spritesheet and animating through the CSS Spritesheet method.

This method involves drawing a spritesheet (spacing every frame of an animation evenly across one large image) and using CSS transforms to cycle through the image.

They benefit from a higher colour range than .GIFS, and I can use vector based SVGs to ensure scalability. That said depending on the amount of frames it could be a huge file.

#####Icons

Started drawing the icons. Feel like icons are much more tedious than HTML/CSS prototypes to construct visually, my normal workflow is to go straight from low fidelity to prototyping. In this instance to cover all colors for the icons I have to export 3 different images in 4 different colors to use, whereas with SVG & CSS I would only need 3 and could change the color on the fly without having to re-open my graphics program.
<br>
<img src="img/icons/icons_1.png" alt=""> 
_Initial Icon draft, not far off what I want for the finished product just need to improve consistency. In partiular the tick icon seems inconsistent compared to the other two. As I added this image I noticed that the tick and the pen before it are not perpindicular, this could be one way to improve consistency._
<br>

Started testing the Icons as SVG to see how the hovers etc would look


----

##__Week Three__

####02/02/2015

#####Personas

Created 3 personas and iterated on the wireframe, as well as the mockups. Decided to add a 'more' or 'notes' feature so users could add small details to tasks.

#####High fidelity Mockups Completion

Completed first Iteration of high fidelity mockups


----

##__Week Four__

####09/02/2015

#####Prototyping

Began moving the HF mockups over to Marvel.

#####Development

Further development of prototype application (HTML, CSS and JS)
Functionality can be ported to a new layout easily if user testing shows current design to be sub-optimal.


----

##__Week Five__

####16/02/2015

#####Prototype testing

#####Development Progress

Development is going a little slower than anticipated. Although angular is extremeley fast for implementing features once it is learned, the concepts and wrokflow have a steep learning curve. At the moment the application has the following functionality:

- Read tasks
- Add a blank task
	- Done for the most part, but appends new tasks in a strange order. Sometimes a task will jump back up in the list, better look into that.
- Some basic animation

At the end of this week I hope to have:

- Add a custom task
- Add a task with the datepicker incorperated
- Begin the filtering by category on the sidebar

Also in regards to the UI, at this stage I also hope to have a blank slate illustration began.


####16/02/2015

Functionality added so far today:

- Add a custom task
- Add categories to tasks
- Added a basic/placeholder date dropdown, just with choices between today and tomorrow

Notes

- Maybe make the default date for new tasks Today, so the user doesnt have to pick it everytime.

Still to do:

- Need to style the inputs so they look...good.
- Need to filter by category! 
	- Have that written somewhere in an old demo of this app somewhere..if only I hadn't lost it!

Overall, the core functionality of the application is very nearly finished!
Plans for tomorrow: more SVG research

----

##__Week Eight__

####07/03/2016

List of what is left to do:

- Find which project I was actually working on
- Unbreak everything (URGH) That didn’t take long, for some reason I had pushed the repo with an empty todo.html directive template
- Write out detailed list of remaining tasks for core functionality before splitting into test interfaces.

__Styling__

- General Style Tasks
- Animate elements on show/hide
- Work on responsiveness.
- Create basic Logo for authenticity purposes.

__Add New Task__

- Seriously, the ‘Add New Task’ bit looks MANKY, need to sort it out. It’s throwing off the look of every other thing.

__Editing Mode Styles__

- Going to need to use ng-class to make the editing more consistent, want to KEEP the hover colour on the background of items being edited. At the moment the background turns white when you mouse off of it.
- Make ‘edit’ and ‘delete’ buttons disappear when in editing mode.
- Also need to sort the labels colours, at the minute they stay white whereas I want them transparent.
- Keep checkbox backgrounds white
- When in editing mode, preview the colour of a category in the dropdown somehow.

__Functionality__

- General Functionality Tasks
- Get started on sidebar sorting. Sort by category or/and date.
- Get $index issue fixed. Thanks for mentioning it by the way Treehouse.

__Editing Mode Functionality Tasks__

- Revert back to previous entry on clicking ‘cancel’
- Only update model on click ’Save' rather than ng-blink

__Add New Task Functionality Tasks__

- Incorporate date picker

__Code Factoring__

- Create separate, smaller directives. 
- New Task
- Todays 
- Tomorrows
- Later
- Sidebar

----

##__Week Ten__

####21/03/2016

Adding in the ability to add categories now, shouldnt be too hard just changing the add task function around.
Now I have to change the adding to dos directive to make it pull how many categories there is dynamicall, shouldnt be too hard.
CAPITALIZED EVERYTHING.

Now need to colour items on the side part, going to need to change everything from specific categories to numbered.

This will actually be kind of awkward since the todo scope and the category scope are two different things. Will have to use the .map function like in this video https://www.youtube.com/watch?v=bCqtb-Z5YGQ    https://www.youtube.com/watch?v=aZtN16YL3l8&spfreload=5   https://www.youtube.com/watch?v=aZtN16YL3l8&spfreload=5

Also going to swap the 'editing' functionality with the sidebar/topbar way of doing things.


####23/03/2016

ARGH. Well that took ALL DAY. Phew, brain dead after that. First foray into moving outside of the basics.
Needed to compare 2 key values in two different arrays and search for a match.
All done now though at long last.
Now to limit the category creation to 12.
Also, to begin working on the filtering functions.


####26/03/2016

Okay now need to filter the stuff by whats on the sidebar.



----

##__Week Ten__

####07/04/2016

Okay now adding the datepicker, making use of the Angular-UI directives.
Looking into dependancy injection for this.
Forgot the controller and the original app declaration are different, was trying to inject into the controller! Derp.
Finally got it in after quite a while but for some reason the datepicker is locked/disabled?

Okay so I had conflicting versions of angularjs and angularui, oops!

Formatting the dates now is a bit weird, have to make dynamic variables for today, tomorrow etc.

####08/04/2016

Okay starting on filtering by date. Unfortunately due to Angular using the word filter as its core thing for manipulating how data is presented, finding information on this is quite tricky.

Okay so I need filter things by today, tomorrow etc. 
Individual days and dates aren't so hard, just take today and add a certain amount. But then I need to filter by date ranges for things like 'next week', 'next month' etc.


````
    $scope.today = new Date();   
    $scope.tomorrow = new Date();
    $scope.nextWeek = new Date();
    $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
    $scope.nextWeek.setDate($scope.nextWeek.getDate() + 7);
````

````
<div>Today: {{today | date:'EEE, dd MMM yyyy'}}</div>
<div>Tomorrow: {{tomorrow | date:'EEE, dd MMM yyyy'}}</div>
<div>Next Week: {{nextWeek | date:'EEE, dd MMM yyyy'}}</div>
````
Then to display them:


NOTE: For the sidebar filters, this looks about right: http://jsfiddle.net/Xesued/Bw77D/7/

Looks like attaching this to an ng-repeat is going to require a custom filter.

custom filters look like this:

    app.filter("taskDate", function() {

    });

What i want this filter to do is basically:

if ($scope.todo.date === $scope.today) {
	return todo
};


Which it turns out when working with dates is harder than I thought.


Date.prototype.sameDay = function(d) {
    return this.getFullYear() === d.getFullYear() && this.getDate() === d.getDate() && this.getMonth() === d.getMonth();
};


// custom filter here
app.filter("taskDate", function() {
    return function(input, date) {
        var todaysDate = new Date();
        var filtered = [];
        angular.forEach(input, function(todo, index) {
            if (todo.date instanceof Date) {
                if (todo.date.sameDay(todaysDate)) {
                    filtered.push(todo);
                }
            } else if (todo.date == date) {
                // filtered.push(todo);
            }
        });

        return filtered;
    };
});

Si far the above code filters specifiaclly for todays date, just need to extend that to tomorrows etc.

Settled for doing seperate filters for each date

app.filter("tomorrowsTasks", function() {
    return function(input, date) {
    var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        var filtered = [];
        angular.forEach(input, function(todo, index) {
            if (todo.date instanceof Date) {
                if (todo.date.sameDay(tomorrow)) {
                    filtered.push(todo);
                }
            } else {}
        });

        return filtered;
    };
});

doesnt feel good repeating code like this but it wont impact development much given that the dates wont be changing. 
(eg today, tomorrow, this week, future.)



####09/04/2016


Lawl today was a bit of a joke. Doing category filters now. Given that dates weren't a factor in this filter and I would just be using the ID properties of the categories it should be much easier.

Hmm, the filter actually is changing to the right digit, so why isn't it working?

Okay so the filter changes to the correct category ID, but yet again the categories set on the items is the string. 
(Eg the task has the cateogry of LIFE but the LIFE category has an ID of 2)

This script here takes two arguments and matches them.
For example

                    {{category.id | grabCatString : categories : 'id' :'category'}}

                    would return the string of the category title, whereas:

                    {{category.id | grabCatString : categories : 'category' :'id'}}

                    Would return the opposite

app.filter('grabCatID', function() {
    return function(native, options, matchField, valueField) {
        if (options) {
            for (var i = 0; i < options.length; i++) {
                var o = options[i][matchField];
                if (o === native) {
                    return options[i][valueField];
                }
            }
        }
        return '';
    };
});

This filter is extremely useful and I can imagine needing to make use of it a lot in the future.



####10/04/2016


Raised an issue on github since ou can't display days as only one letter (M for monday etc)
TRied fooling it using ::first-letter
but can only use that on block or inline-block elements
and the way the things were structured, that meant I could do display:none on the whole thing
and then use ::first-letter
becuase it needed to be inline-block derp

Site is now responsive woo. couldnt use any bootstrap native forms without adding jquery, but angular was capable bud



form validation

formname.inputname.validvalue
eg:

{{addTodo.taskName.$valid}}

So users cant enter a task that doesnt have an assigned name, cataegory or date.
